import React from 'react';
import './styles/app.css';
import Header from "./components/Header";
import Hero from "./components/Hero";
import Description from "./components/Description";

const App = () => {
  return (
  <div>
      <div className="bg-white p-6 border shadow">
          <Header />
      </div>
      <div className="relative px-5">
          <Hero />
          {/*<Description />*/}
      </div>
  </div>
  );
};

export default App;
