import React, { Fragment } from "react";
import { ReactComponent as LockIcon } from "feather-icons/dist/icons/lock.svg";
import { ReactComponent as QuotesLeftIconBase } from "../images/quotes-l.svg";
import { ReactComponent as SvgCoronavirus } from "../images/coronavirus-1.svg";

const Background = () => {
    return (
        <Fragment>
            <SvgCoronavirus className="pointer-events-none opacity-5 absolute right-0 bottom-auto h-16 w-16 transform -translate-x-1/3 translate-y-64 -z-10" />
            <SvgCoronavirus className="pointer-events-none opacity-5 absolute right-0 bottom-auto h-32 w-32 transform -translate-x-10 -translate-y-32 -z-10" />
            <SvgCoronavirus className="pointer-events-none opacity-5 absolute left-auto bottom-0 h-64 w-64 transform -translate-x-2/3 -z-10" />
        </Fragment>
    )
};

export default () => {
    const imageSrc = "https://images.unsplash.com/photo-1573496359142-b8d87734a5a2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80";

    return (
            <div className="flex flex-col lg:flex-row lg:items-center max-w-screen-xl mx-auto pt-20">
                <div className="relative lg:w-5/12 text-center max-w-lg mx-auto lg:mb-24 lg:mb-48 lg:max-w-none lg:text-left">
                    <h1 className="font-bold text-2xl md:text-2xl lg:text-3xl xl:text-4xl text-gray-900 leading-tight">
                        Send and receive anonymous trust verified information to help your community.
                    </h1>
                    <p className="my-5 lg:my-8 text-base xl:text-lg">
                        Please enter your mobile number below. You will receive a text with a 4-digit code to activate
                        your account. It's that simple and its all anonymous!
                    </p>
                    <div className="relative max-w-md text-center mx-auto lg:mx-0">
                        <input
                            className="sm:pr-48 pl-8 py-4 sm:py-5 rounded border-2 w-full font-medium focus:outline-none transition duration-300  focus:border-primary-500 hover:border-gray-500"
                            type="text"
                            placeholder="Your unique Bluetooth code"
                        />
                        <button
                            className="w-full sm:absolute right-0 top-0 bottom-0 bg-primary-500 text-gray-100 font-bold mr-2 my-4 sm:my-2 rounded py-4 flex items-center justify-center sm:w-40 sm:leading-none focus:outline-none hover:bg-primary-900 transition duration-300"
                        >
                            Start Alert
                        </button>
                    </div>
                    <div className="flex flex-row align-middle max-w-md text-center mx-auto lg:mx-0 my-3 lg:my-4">
                        <LockIcon className="w-8 h-8 pr-2" /> <span className="pt-1">This application is secure and anonymous</span>
                    </div>
                </div>
                <div className='relative mt-12 lg:mt-0 flex-1 flex flex-col justify-center lg:self-end'>
                    <div className="flex justify-center flex-wrap lg:justify-end items-center">
                        <img className="max-w-full w-8/12 rounded-t sm:rounded relative z-20" src={imageSrc} alt="testimonial"/>
                        <div className="max-w-sm rounded-b md:rounded-none relative sm:absolute bottom-0 z-20 px-8 py-6 sm:px-10 sm:py-8 bg-secondary-900 text-gray-400 font-medium md:-translate-x-32 text-sm leading-relaxed md:-mr-16 xl:mr-0">
                            <QuotesLeftIconBase className="w-16 h-16 md:w-12 md:h-12 sm:w-8 sm:h-8 lg:absolute top-0 left-0 text-gray-100 md:text-red-500 transform translate-x-1 md:-translate-x-1/2 md:-translate-y-5 opacity-10 md:opacity-100"/>
                            <blockquote className="font-bold">With CovidTracker I was able to be notified that I was in close proximity to someone with the disease. Seeing
                            that the response was verified and anonymous meant that I could act quickly and efficiently. Its so easy!</blockquote>
                            <p className="mt-4 font-bold">Jane Smith</p>
                            <p className="mt-1 text-sm text-gray-500">Ireland</p>
                        </div>
                    </div>
                </div>
                <Background/>
            </div>
    );
};
