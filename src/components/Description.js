import React, { Fragment } from "react";
import { ReactComponent as LockIcon } from "feather-icons/dist/icons/lock.svg";
import { ReactComponent as QuotesLeftIconBase } from "../images/quotes-l.svg"
import { ReactComponent as SvgCoronavirus } from "../images/coronavirus-1.svg";
import fbLogo from '../images/logo-fb.png';
import linkedinlogo from '../images/logo-linkedin.png';
import twitterlogo from '../images/logo-twitter.png';
import googleLogo from '../images/logo-google.png';


const Background = () => {
    return (
        <Fragment>
            <SvgCoronavirus className="pointer-events-none opacity-5 absolute right-0 bottom-auto h-16 w-16 transform -translate-x-1/3 translate-y-64 -z-10" />
            <SvgCoronavirus className="pointer-events-none opacity-5 absolute right-0 bottom-auto h-32 w-32 transform -translate-x-10 -translate-y-32 -z-10" />
            <SvgCoronavirus className="pointer-events-none opacity-5 absolute left-auto bottom-0 h-64 w-64 transform -translate-x-2/3 -z-10" />
        </Fragment>
    )
};

export default () => {

    return (
        // <div className="max-w-sm w-full lg:max-w-full lg:flex">
        <div className="h-screen flex justify-center items-center pb-10">
            <div className="max-w-2xl overflow-hidden rounded shadow-lg mx-auto lg:flex" style={{ minHeight: '500px'}}>
                <div
                    className="h-48 lg:h-auto lg:w-1/2 flex-none bg-cover bg-secondary-900 rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
                >
                </div>
                <div
                    className="bg-white rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
                    <div className="mb-8">
                        <div className="text-gray-900 font-bold text-xl mb-2">Can coffee make you a better developer?</div>
                        <p className="text-gray-700 text-base">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.</p>
                        transition
                    </div>
                    <div className="flex items-center">

                    </div>
                </div>
            </div>
        </div>
    );
};
