import React from "react";
import { motion } from "framer-motion";
// import tw from "twin.macro";
// import styled from "styled-components";
// import { css } from "styled-components/macro"; //eslint-disable-line

import useAnimatedNavToggler from "../helpers/useAnimatedNavToggler.js";

import logo from "../images/logo.svg";
import { ReactComponent as MenuIcon } from "feather-icons/dist/icons/menu.svg";
import { ReactComponent as CloseIcon } from "feather-icons/dist/icons/x.svg";

// export const NavLinks = tw.div`inline-block`;

/* hocus: stands for "on hover or focus"
 * hocus:bg-primary-700 will apply the bg-primary-700 class on hover or focus
 */
// export const NavLink = tw.a`
//   text-lg my-2 lg:text-sm lg:mx-6 lg:my-0
//   font-semibold tracking-wide transition duration-300
//   pb-1 border-b-2 border-transparent hover:border-primary-500 hocus:text-primary-500
// `;

// export const PrimaryLink = tw(NavLink)`
//   lg:mx-0
//   px-8 py-3 rounded bg-primary-500 text-gray-100
//   hocus:bg-primary-700 hocus:text-gray-200 focus:shadow-outline
//   border-b-0
// `;
//
// export const LogoLink = styled(NavLink)`
//   ${tw`flex items-center font-black border-b-0 text-2xl! ml-0!`};
//
//   img {
//     ${tw`w-10 mr-3`}
//   }
// `;

// export const MobileNavLinksContainer = tw.nav`flex flex-1 items-center justify-between`;
// export const NavToggle = tw.button`
//   lg:hidden z-20 focus:outline-none hocus:text-primary-500 transition duration-300
// `;
// export const MobileNavLinks = motion.custom(styled.div`
//   ${tw`lg:hidden z-10 fixed top-0 inset-x-0 mx-4 my-6 p-8 border text-center rounded-lg text-gray-900 bg-white`}
//   ${NavLinks} {
//     ${tw`flex flex-col items-center`}
//   }
// `);

export default ({ roundedHeaderButton = false, logoLink, links, className, collapseBreakpointClass = "lg" }) => {
    /*
     * This header component accepts an optionals "links" prop that specifies the links to render in the navbar.
     * This links props should be an array of "NavLinks" components which is exported from this file.
     * Each "NavLinks" component can contain any amount of "NavLink" component, also exported from this file.
     * This allows this Header to be multi column.
     * So If you pass only a single item in the array with only one NavLinks component as root, you will get 2 column header.
     * Left part will be LogoLink, and the right part will be the the NavLinks component you
     * supplied.
     * Similarly if you pass 2 items in the links array, then you will get 3 columns, the left will be "LogoLink", the center will be the first "NavLinks" component in the array and the right will be the second "NavLinks" component in the links array.
     * You can also choose to directly modify the links here by not passing any links from the parent component and
     * changing the defaultLinks variable below below.
     * If you manipulate links here, all the styling on the links is already done for you. If you pass links yourself though, you are responsible for styling the links or use the helper styled components that are defined here (NavLink)
     */

    const { showNavLinks, animation, toggleNavbar } = useAnimatedNavToggler();
    const collapseBreakpointCss = collapseBreakPointCssMap["lg"];

    const defaultLinks = [
        <div className={ showNavLinks ? "flex flex-col items-center" : "inline-block"} key={1}>
            <a className="text-lg my-2 lg:text-sm lg:mx-6 lg:my-0 font-semibold tracking-wide transition duration-300 pb-1 border-b-2 border-transparent hover:border-primary-500 hover:text-primary-500" href="/#">How It Works</a>
            <a className="text-lg my-2 lg:text-sm lg:mx-6 lg:my-0 font-semibold tracking-wide transition duration-300 pb-1 border-b-2 border-transparent hover:border-primary-500 hover:text-primary-500" href="/#">About Us</a>
            <a className="text-lg my-2 lg:text-sm lg:mx-6 lg:my-0 font-semibold tracking-wide transition duration-300 pb-1 border-b-2 border-transparent hover:border-primary-500 hover:text-primary-500" href="/#">Contact Us</a>
            <a className="lg:mx-0 px-8 py-3 font-semibold rounded bg-primary-500 text-gray-100 hover:bg-primary-900 hover:text-gray-200 focus:shadow-outline border-b-0" href="/#">Send Covid Alert</a>
        </div>
    ];

    const defaultLogoLink = (
        <a className="flex items-center border-b-0 ml-0!" href="/">
            <h1 className=" font-black text-2xl md:text-2xl lg:text-3xl xl:text-4xl text-secondary-900 leading-tight">
                Covid
            </h1>
            <h1 className="text-2xl md:text-2xl lg:text-3xl xl:text-4xl text-primary-900 leading-tight">
                Tracker
            </h1>
        </a>
    );

    return (
        <header className="header-light flex justify-between items-center max-w-screen-xl mx-auto">
            <nav className={`hidden lg:flex flex-1 justify-between items-center ${collapseBreakpointCss.desktopNavLinks}`}>
                {defaultLogoLink}
                {defaultLinks}
            </nav>

            <nav className={`flex flex-1 items-center justify-between ${collapseBreakpointCss.mobileNavLinksContainer}`}>
                {defaultLogoLink}
                <motion.div animate={animation}  initial={{ x: "150%", display: "none" }} className={`lg:hidden z-10 fixed top-0 inset-x-0 mx-4 my-6 p-8 border text-center rounded-lg text-gray-900 bg-white ${collapseBreakpointCss.mobileNavLinks}`}>
                    {defaultLinks}
                </motion.div>
                <button onClick={toggleNavbar} className={showNavLinks ? "open lg:hidden z-20 focus:outline-none hocus:text-primary-500 transition duration-300" : "closed lg:hidden z-20 focus:outline-none hocus:text-primary-500 transition duration-300"}>
                    {showNavLinks ? <CloseIcon className="w-6 h-6" /> : <MenuIcon className="w-6 h-6" />}
                </button>
            </nav>
        </header>
    );
};

/* The below code is for generating dynamic break points for navbar.
 * Using this you can specify if you want to switch
 * to the toggleable mobile navbar at "sm", "md" or "lg" or "xl" above using the collapseBreakpointClass prop
 * Its written like this because we are using macros and we can not insert dynamic variables in macros
 */

const collapseBreakPointCssMap = {
    sm: {
        mobileNavLinks: "sm:hidden",
        desktopNavLinks: "sm:flex",
        mobileNavLinksContainer: "sm:hidden"
    },
    md: {
        mobileNavLinks: "md:hidden",
        desktopNavLinks: "md:flex",
        mobileNavLinksContainer: "md:hidden"
    },
    lg: {
        mobileNavLinks: "lg:hidden",
        desktopNavLinks: "lg:flex",
        mobileNavLinksContainer: "lg:hidden"
    },
    xl: {
        mobileNavLinks: "lg:hidden",
        desktopNavLinks: "lg:flex",
        mobileNavLinksContainer: "lg:hidden"
    }
};
